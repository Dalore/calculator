﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        double diametr = 0;

        double length = 0;

        double flow = 0;

        double viscosity = 0;

        double roughness = 0;

        double lyambda1 = 0;

        double h = 0;

        string s = "";

        string error = "Внимание, поля не должны быть пустыми, а введенные значения не должны равняться нулю";


        public MainWindow()
        {
            InitializeComponent();
        }

        private void V_Click(object sender, RoutedEventArgs e)
        {
            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), "Resources/v.jpg"));
            Window w = new Window();
            w.Title = "Таблица вязкости";
            w.Background = myBrush;
            w.Height = 400;
            w.Width = 600;
            w.Show();
        }

        private void S_Click(object sender, RoutedEventArgs e)
        {
            ImageBrush myBrush = new ImageBrush();
            myBrush.ImageSource = new BitmapImage(new Uri(BaseUriHelper.GetBaseUri(this), "Resources/s.jpg"));
            Window w = new Window();
            w.Title = "Таблица шероховатости";
            w.Background = myBrush;
            w.Height = 400;
            w.Width = 600;
            w.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            diametr = Convert.ToDouble(Di.Text);

            length = Convert.ToDouble(Dl.Text);

            flow = Convert.ToDouble(Ra.Text);

            viscosity = Convert.ToDouble(Vy.Text);

            roughness = Convert.ToDouble(SH.Text);

            if (diametr != 0 && length != 0 && flow != 0 && viscosity != 0 && roughness != 0)
            {

                double A = (Math.PI * diametr * diametr) / 4000000.0;

                double V = (flow / A) / 60000.0;

                double Re = ((V * 4 * (diametr / 4)) / viscosity) / 1000;

                if (Re < 2000)
                {
                    s = "Ламинарный.";
                    lyambda1 = (64 / Re);
                }
                else if (Re > 2000 && Re < 4000)
                {
                    s = "Переходный.";
                    lyambda1 = (2.7 / (Math.Pow(Re, 0.53)));
                }
                else
                {
                    s = "Турбулентный.";
                    lyambda1 = 0.11 * (Math.Pow((roughness / diametr) + (60 / Re), 0.25));
                }

                h = lyambda1 * (length / diametr) * ((V * V) / 2 * 9.88);
                var result = MessageBox.Show("Площадь сечения трубопровода: " + A + ".\nСкорость течения жидкости: " + V + ".\nЧисло Рейнольдса: " + Re + "\nРежим течения: " + s + "\nКоэффициент гидравлического трения: " + lyambda1 + ".\n\nПотери напора по длине: " + h + "." + "\n\nЖелаете вбить новые данные?",
                    "Уведомление",
                    MessageBoxButton.YesNo);


                if (result == MessageBoxResult.Yes)
                {
                    Di.Text = String.Empty;

                    Dl.Text = String.Empty;

                    Ra.Text = String.Empty;

                    Vy.Text = String.Empty;

                    SH.Text = String.Empty;
                }
                else Application.Current.Shutdown();
            }
            else MessageBox.Show(error,"Ошибка");
        }

        private void Di_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!Di.Text.Contains(",")
              && Di.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
        private void Dl_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!Dl.Text.Contains(",")
              && Dl.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
        private void Ra_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!Ra.Text.Contains(",")
              && Ra.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
        private void Vy_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!Vy.Text.Contains(",")
              && Vy.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
        private void SH_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!SH.Text.Contains(",")
              && SH.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }
    }
}
